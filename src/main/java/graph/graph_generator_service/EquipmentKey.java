package graph.graph_generator_service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@AllArgsConstructor
@EqualsAndHashCode
// A class that stores key information about a node
public class EquipmentKey implements Cloneable {

    private String tagPipe;

    // Standard clone method.
    public EquipmentKey clone() {
        return new EquipmentKey(this.tagPipe);
    }

    @Override
    // Standard toString method.
    public String toString() {
        return String.format("[tagPipe: %s]", tagPipe);
    }
}
