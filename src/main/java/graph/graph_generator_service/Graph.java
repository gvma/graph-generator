package graph.graph_generator_service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

import org.jgrapht.graph.DirectedMultigraph;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.extern.java.Log;
import org.jgrapht.traverse.DepthFirstIterator;
import org.jgrapht.traverse.GraphIterator;

@Getter
@EqualsAndHashCode(callSuper = false)
@Log
// A class that represents our graph.
public class Graph extends DirectedMultigraph<NavalNode, NavalEdge> {
    private static final long serialVersionUID = 1L;

    
    protected Set<EquipmentKey> inputs, outputs;

    // Using the super class constructor and initializing inputs and outputs.
    public Graph() {
        super(NavalEdge.class);
        init();
    }

    /*
     * The super class constructor which holds initialization of inputs and outputs.
     * 
     * @param csvPath The path extracted from the arguments.
     */
    public Graph(String csvPath) throws IOException {
        super(NavalEdge.class);
        init();
        if (csvPath != null) {
            GraphGenerator generator = new GraphGenerator(csvPath, null);
            generator.populateGraph(this);
        } else {
            throw new IOException("CSV path can not be null!");
        }
    }

    /*
     * The super class constructor which holds initialization of inputs and outputs.
     * @param csvPath The path extracted from the arguments.
     * @param drawings A Set containing all whitelisted drawings.
     */
    public Graph(String csvPath, Set<String> drawings) throws IOException {
        super(NavalEdge.class);
        init();
        if (csvPath != null) {
            GraphGenerator generator = new GraphGenerator(csvPath, drawings);
            generator.populateGraph(this);
        }
    }

    // Method to check if all keys are present in a vertexSet
    protected Set<EquipmentKey> checkAllExists(Set<EquipmentKey> keys, Set<NavalNode> vertexSet) {
        try {
            for (EquipmentKey key : keys) {
                findNode(key, vertexSet);
            }
            return keys;
        } catch (NoSuchElementException e) {
            log.warning(e.getMessage());
            return null;
        }
    }

    // Standard set method to set the inputs
    protected void setInputs(Set<EquipmentKey> inputs, Set<NavalNode> vertexSet) {
        this.inputs = checkAllExists(inputs, vertexSet);
    }

    // Standard set method to set the outputs
    protected void setOutputs(Set<EquipmentKey> outputs, Set<NavalNode> vertexSet) {
        this.outputs = checkAllExists(outputs, vertexSet);
    }

    // A function used to find the nodes corresponding to each given key
    public Set<NavalNode> keysToNode(Set<EquipmentKey> keys) {
        Set<NavalNode> nodes = new HashSet<>();
        for (EquipmentKey key : keys) {
            nodes.add(findNode(key));
        }
        return nodes;
    }

    // A function used in the constructor, to init two class attributes.
    private void init() {
        inputs = new HashSet<>();
        outputs = new HashSet<>();
    }

    // Standard clone method.
    public Graph clone() {
        Graph graph = this.cloneWithoutEdges();

        for (NavalEdge edge : edgeSet()) {
            NavalNode source = getEdgeSource(edge);
            NavalNode target = getEdgeTarget(edge);
            graph.addEdge(source.clone(), target.clone());
        }

        return graph;
    }

    /*
     * A method designed to clone only the graph's nodes.
     * @return Returns a graph with new instances of nodes.
     */
    public Graph cloneWithoutEdges() {
        Graph graph = new Graph();

        graph.setInputs(inputs, vertexSet());
        graph.setOutputs(outputs, vertexSet());
        for (NavalNode vertex : vertexSet()) {
            graph.addVertex(vertex.clone());
        }

        return graph;
    }

    /*
     * A method created to add edges in the graph.
     * @param source The source node of the edge.
     * @param target The target node of the edge.
     * @param graph The graph that holds both nodes.
     * @return Nothing
     */
    public void addEdge(NavalNode source, NavalNode target, Graph graph) {
        if (graph.containsEdge(source, target) && !containsEdge(source, target)) {
            addEdge(source, target);
            if (graph.containsEdge(target, source) && !containsEdge(target, source)) {
                addEdge(target, source);
            }
        }
    }

     // A method designed to remove all the edges from this graph.
    public void clearEdges() {
        Set<NavalEdge> edges = copyEdges(edgeSet());
        for (NavalEdge edge : edges) {
            removeEdge(getEdgeSource(edge), getEdgeTarget(edge));
        }
    }

    // A method that reports information about this graph.
    public void report() {
        System.out.println("Subgraph properties:");
        System.out.println("    > Nodes: " + this.vertexSet().size());
        System.out.println("    > Edges: " + this.edgeSet().size());
    }

    /*
     * A method that returns a node from the graph based in its key.
     * @param key The EquipmentKey class that is a node's key.
     * @return Returns the node from this graph, if found, or null if it is not.
     */
    public NavalNode findNode(EquipmentKey key) {
        return findNode(key, this.vertexSet());
    }

    /*
     * A method that returns a node from the graph based in its key.
     * @param key The EquipmentKey class that is a node's key.
     * @param vertexSet The graph's vertex set.
     * @return Returns the node from this graph, if found, or null if it is not.
     */
    public NavalNode findNode(EquipmentKey key, Set<NavalNode> vertexSet) {
        try {
            if (vertexSet == null) {
                vertexSet = this.vertexSet();
            }
            return vertexSet.stream().filter((tp) -> (tp.checkKey(key))).findAny().get();
        } catch (NoSuchElementException e) {
            log.warning(String.format("Node with key %s not found!", key));
            return null;
        }
    }

    /*
     * A method designed to retrieve the subgraph, given start, end and whitelisted drawings.
     * @param start The starting node in the complete graph.
     * @param end The endpoint node in the complete graph.
     * @return Returns the subgraph created from the starting node to the endpoint.
     */
    public Graph getSubgraph(EquipmentKey start, EquipmentKey end) {
        Set<EquipmentKey> inputs = new HashSet<>();
        Set<EquipmentKey> outputs = new HashSet<>();

        inputs.add(start);
        outputs.add(end);
        return getSubgraph(inputs, outputs);
    }
    /*
     * A method designed to retrieve the subgraph, given start, end and whitelisted drawings.
     * @param inputs All starting nodes in the complete graph.
     * @param outputs All endpoint nodes in the complete graph.
     * @return Returns the subgraph created from the starting node to the endpoint.
     */
    public Graph getSubgraph(Set<EquipmentKey> inputs, Set<EquipmentKey> outputs) {
        // Preparing a new object for the subgraph
        Graph subgraph = new Graph();
        subgraph.setInputs(inputs, this.vertexSet());
        subgraph.setOutputs(outputs, this.vertexSet());
        for (NavalNode node : keysToNode(subgraph.getOutputs())) {
            subgraph.addVertex(node);
        }

        log.info("Finding all paths!");
        long startTime = System.nanoTime();

        GraphGenerator generator = new GraphGenerator();
        for (EquipmentKey input : inputs) {
            NavalNode inputNode = findNode(input);
            Set<Integer> memory = new HashSet<>();
            generator.DFS(inputNode, this, subgraph, memory);
        }

        long endTime = System.nanoTime();
        log.info("Execution time in seconds: " + ((endTime - startTime) * (1e-9)));
        return subgraph;
    }

    /*
     * A method designed to get the source adjacency list from a given node.
     * @param node The node that will be used to retrieve the source adjacency list.
     * @return Returns a set containing all source edges.
     */
    public Set<NavalNode> getSourceList(NavalNode node) {
        Set<NavalEdge> edges = incomingEdgesOf(node);
        Set<NavalNode> adjList = new HashSet<>();
        for (NavalEdge edge : edges) {
            adjList.add(getEdgeSource(edge));
        }
        return adjList;
    }

    /*
     * A method designed to get the target adjacency list from a given node.
     * @param node The node that will be used to retrieve the target adjacency list.
     * @return Returns a set containing all target edges.
     */
    public Set<NavalNode> getTargetList(NavalNode node) {        
        Set<NavalEdge> edges = outgoingEdgesOf(node);
        Set<NavalNode> adjList = new HashSet<>();
        for (NavalEdge edge : edges) {
            adjList.add(getEdgeTarget(edge));
        }
        return adjList;
    }

    /*
     * A method designed to get the source and target adjacency list from a given node.
     * @param node The node that will be used to retrieve the source and target adjacency list.
     * @return Returns a set containing all source and target edges.
     */
    public Set<NavalNode> getAdjList(NavalNode node) {
        Set<NavalNode> targets = getTargetList(node);
        Set<NavalNode> sources = getSourceList(node);
        targets.addAll(sources);
        return targets;
    }

    // A method to create a new Set of nodes and avoid work with reference
    public Set<NavalNode> copyNodes(Set<NavalNode> nodes) {
        Set<NavalNode> tmpSet = new HashSet<>();
        for (NavalNode tmp : nodes) {
            tmpSet.add(tmp);
        }
        return tmpSet;
    }

    // A method to create a new Set of edges and avoid work with reference
    public Set<NavalEdge> copyEdges(Set<NavalEdge> edges) {
        Set<NavalEdge> newEdges = new HashSet<>();
        for (NavalEdge edge : edges) {
            newEdges.add(getEdge(getEdgeSource(edge), getEdgeTarget(edge)));
        }
        return newEdges;
    }

    // A function to remove vertices using a Set of vertex
    public void removeVertices(Set<NavalNode> nodes) {
        for (NavalNode node : copyNodes(nodes)) {
            removeVertex(node);
        }
    }

    // A function to remove all vertices with degree 0
    public void clearFreeVertices() {
        for (NavalNode node : copyNodes(vertexSet())) {
            if (degreeOf(node) == 0 || (outDegreeOf(node) == 0 && !isOutput(node))) {
                removeVertex(node);
            }
        }

        // Cleaning inputs
        Iterator<EquipmentKey> iter = inputs.iterator();
        while (iter.hasNext()) {
            if (findNode(iter.next()) == null) {
                iter.remove();
            }
        }

        // Cleaning outputs
        iter = outputs.iterator();
        while (iter.hasNext()) {
            if (findNode(iter.next()) == null) {
                iter.remove();
            }
        }
    }

    // A function to check if all given nodes are outputs
    public Boolean isOutput(NavalNode... nodes) {
        for (NavalNode node : nodes) {
            if (outputs.contains(node.getKey())) {
                return true;
            }
        }
        return false;
    }

    // A function to check if a given node is inside a cycle
    public Boolean isCycle(NavalNode node, Set<NavalNode> memory, Graph auxGraph) {
        // Reach output, don't remove
        if (isOutput(node)) {
            return false;
        }

        Boolean cycle = true;
        for (NavalNode target : getTargetList(node)) {
            if (!memory.contains(target)) {
                // Cycle
                Set<NavalNode> tmpMemory = copyNodes(memory);
                tmpMemory.add(node);
                // Checking each way
                cycle = cycle && isCycle(target, tmpMemory, auxGraph);
            }
        }
        return cycle;
    }

    // A function to find all edges without cycles
    public void edgesWithoutCycles(NavalNode node, Set<NavalNode> memory, Graph auxGraph) {
        for (NavalNode target : getTargetList(node)) {
            if (!memory.contains(target)) {
                Set<NavalNode> tmpMemory = copyNodes(memory);
                tmpMemory.add(target);
                edgesWithoutCycles(target, tmpMemory, auxGraph);
            }

            if (memory.contains(target) || auxGraph.containsEdge(node, target)) {
                continue;
            }

            // Initial condition to check cycles
            if (!isCycle(target, memory, auxGraph)) {
                auxGraph.addEdge(node, target, this);
            }
        }
    }

    // A function to clear all cycles of the graph
    public void clearCycles() {
        Set<NavalNode> memory = new HashSet<>();

        // Auxiliar variable to handle edges
        Graph tmpGraph = this.cloneWithoutEdges();

        for (EquipmentKey start : inputs) {
            NavalNode input = findNode(start);
            if (input != null) {
                memory.add(input);
                edgesWithoutCycles(input, memory, tmpGraph);
            }
        }

        // Copying all edges back to this object
        clearEdges();
        for (NavalEdge edge : tmpGraph.edgeSet()) {
            addEdge(getEdgeSource(edge), getEdgeTarget(edge));
        }
        clearFreeVertices();
    }

    public Set<NavalNode> getConnectors() {
        Set<NavalNode> nodesToRemove = new HashSet<>();
        for (NavalNode node : this.vertexSet()) {
            if (node.isConnector(this)) {
                nodesToRemove.add(node);
            }
        }
        return nodesToRemove;
    }

    // A function to clear all connectors of the graph
    public void clearConnectors() {
        Set<NavalNode> nodesToRemove = getConnectors();
        for (NavalNode node : nodesToRemove) {
            for (NavalNode target : getTargetList(node)) {
                for (NavalNode source : getSourceList(node)) {
                    if (!this.containsEdge(source, target) && !source.equals(target)) {
                        this.addEdge(source, target);
                    }
                }
            }
        }

        removeVertices(nodesToRemove);
    }

    // A function to clear all pipe loops
    public void clearPipeLoops() {
        Set<NavalNode> clearList = new HashSet<>();
        for (NavalNode node : vertexSet()) {
            // Only exist fork with more than one exit
            if (clearList.contains(node) || degreeOf(node) < 3) {
                continue;
            }

            for (NavalNode target : getTargetList(node)) {
                Set<NavalNode> adjs = getAdjList(target);
                if (adjs.size() == 2) {
                    List<NavalNode> list = new ArrayList<NavalNode>(adjs);
                    NavalNode adj1 = list.get(0);
                    NavalNode adj2 = list.get(1);
                    if (containsEdge(adj1, adj2) || containsEdge(adj2, adj1)) {
                        clearList.add(target);
                    }
                }
            }
        }
        removeVertices(clearList);
    }

    public Set<NavalNode> getComponents() {
        Set<NavalNode> nodesToRemove = new HashSet<>();
        for (NavalNode node : this.vertexSet()) {
            if (node.isComponent(this)) {
                nodesToRemove.add(node);
            }
        }
        return nodesToRemove;
    }

    public Set<NavalNode> redirectComponentEdges() {
        Set<NavalNode> nodesToRemove = getComponents();
        for (NavalNode node : nodesToRemove) {
            for (NavalNode target : getTargetList(node)) {
                for (NavalNode source : getSourceList(node)) {
                    if (!this.containsEdge(source, target) && !source.equals(target)) {
                        this.addEdge(source, target);
                        source.getFittings().add(node);
                        source.getFittings().addAll(node.getFittings());
                    }
                }
            }
        }

        removeVertices(nodesToRemove);
        return nodesToRemove;
    }

    public void mergeEdges(NavalNode previous) {
        for (NavalNode source : getSourceList(previous)) {
            for (NavalNode target : getTargetList(previous)) {
                if (!this.containsEdge(source, target) && !source.equals(target)) {
                    this.addEdge(source, target);
                }
            }
        }
    }

    public void mergeEqualPiperun(NavalNode previous) {
        Set<NavalNode> sourceNodes = new HashSet<>();
        sourceNodes.add(previous);
        mergeEqualPiperun(sourceNodes);
    }

    public void mergeEqualPiperun(Set<NavalNode> sourceNodes) {
        for (NavalNode previous : sourceNodes) {
            GraphIterator<NavalNode, NavalEdge> iterator = new DepthFirstIterator<>(this, previous);
            iterator.next();
            while (iterator.hasNext()) {
                NavalNode next = iterator.next();
                if (next.getTagPipe().equals(previous.getTagPipe())) {
                    Set<NavalNode> targetList = getTargetList(previous);
                    if (targetList.size() == 1) {
                        if (!inputs.contains(previous.getKey())) {
                            mergeEdges(previous);
                            removeVertex(previous);
                        }
                    }
                }
                previous = next;
            }
        }
    }

    public Set<NavalNode> equipmentKeyInputsToNavalNode() {
        Set<NavalNode> start = new HashSet<>();
        for (EquipmentKey key : inputs) {
            start.add(this.findNode(key));
        }
        return start;
    }

    // A function to execute a workflow for clear the graph
    public Graph clear() {
        return clear(100); // Max level
    }
    
    // A function to execute a workflow for clear the graph
    public Graph clear(Integer level) {
        if (level > 0) {
            this.clearConnectors();
            this.clearCycles();
        }
        if (level > 1) {
            this.clearPipeLoops();
        }
        if (level > 2) {
            this.redirectComponentEdges();
        }
        if (level > 3) {
            this.joinNodes();
            this.mergeEqualPiperun(equipmentKeyInputsToNavalNode());
        }
        return this;
    }

    // A function to merge two given nodes
    public void merge(NavalNode source, NavalNode target) {
        for (NavalNode targetFinal : getTargetList(target)) {
            if (targetFinal != source) {
                addEdge(source, targetFinal);
            }
        }

        for (NavalNode sourceFinal : getSourceList(target)) {
            if (sourceFinal != source) {
                addEdge(sourceFinal, source);
            }
        }

        removeVertex(target);
    }

    // A function to merge nodes of the same type in a row, using a starting node
    public void joinNodes(NavalNode node, Set<NavalNode> memory) {
        Set<NavalNode> targets = getTargetList(node);
        Set<NavalNode> allAdjs = getAdjList(node);

        for (NavalNode target : targets) {
            if (memory.contains(target) || isOutput(target)) {
                continue;
            }

            Set<NavalNode> allAdjsTarget = getAdjList(target);
            if (node.getTagPipe().equals(target.getTagPipe()) && allAdjs.size() == 2 && allAdjsTarget.size() == 2) {
                merge(node, target);
                joinNodes(node, memory);
                memory.add(node);
            } else {
                memory.add(node);
                joinNodes(target, memory);
            }
        }
        memory.add(node);
    }

    // A function to merge nodes of the same type in a row
    public void joinNodes() {
        for (EquipmentKey start : inputs) {
            NavalNode startNode = findNode(start);
            for (NavalNode target : getTargetList(startNode)) {
                joinNodes(target, new HashSet<>());
            }
        }
    }

}