package graph.graph_generator_service;

import lombok.Getter;
import lombok.extern.java.Log;

import java.io.*;
import java.util.*;

@Getter
@Log
// A class that creates the graph from the csv acquired from the input and
// generates the subgraph
public class GraphGenerator {

    private String csvPath;
    private BufferedReader bufferedReader;
    int vertexCount = 1, timer = 0;
    private Set<String> drawings = new HashSet<>();
    private Set<EquipmentKey> from;
    private Set<EquipmentKey> to;

    // Sole constructor.
    public GraphGenerator() {
    }

    /*
     * A constructor that builds an instance of GraphGenerator.
     * 
     * @param csvPath The path extracted from the arguments.
     * 
     * @param drawings A Set containing all whitelisted drawings.
     * 
     * @exception FileNotFoundException When the file is not found with the csvPath
     * argument.
     */
    public GraphGenerator(String csvPath, Set<String> drawings) throws FileNotFoundException {
        try {
            this.csvPath = csvPath;
            this.drawings = drawings;
            bufferedReader = new BufferedReader(new FileReader(this.csvPath));
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("File not found!");
        }
    }

    /*
     * A helper method to create graph nodes.
     * 
     * @param graph The graph that is being created.
     * 
     * @param map A map which stores all created nodes.
     * 
     * @param data An array containing a line extracted from the CSV.
     * 
     * @param nodeIndexes The array positions to separate new nodes from one CSV
     * line.
     * 
     * @return Nothing
     */
    public void existsAndPut(Graph graph, Map<String, NavalNode> map, String[] data, int... nodeIndexes) {
        if (map.get(data[nodeIndexes[1]]) == null) {
            NavalNode navalNode = new NavalNode(data[nodeIndexes[0]], data[nodeIndexes[1]], data[nodeIndexes[2]],
                    data[nodeIndexes[3]], data[nodeIndexes[4]], (long) vertexCount++);
            map.put(data[nodeIndexes[1]], navalNode);
            graph.addVertex(navalNode);
        }
    }

    public void existsAndPutPipe(Graph graph, Map<String, NavalNode> map, String[] data, int... nodeIndexes) {
        String pipeType = "Primary Piping";
        String type = pipeType;
        if (data[6].equals("OPC")) {
            pipeType = "Connector";
        }
        if (map.get(data[nodeIndexes[1]]) == null) {
            NavalNode navalNode = new NavalNode(data[nodeIndexes[0]], data[nodeIndexes[1]], pipeType,
                    data[nodeIndexes[2]], type, (long) vertexCount++);
            map.put(data[nodeIndexes[1]], navalNode);
            graph.addVertex(navalNode);
        }
    }

    /*
     * A method to generate the graph from the CSV.
     * 
     * @param graph The graph that is being created.
     * 
     * @return Returns true whether the graph was created, false if it was not.
     * 
     * @exception When for some reason, the file could not be read.
     */
    public boolean populateGraph(Graph graph) throws IOException {
        Map<String, NavalNode> map = new HashMap<>();
        bufferedReader.readLine(); // Read first line of the CSV (not necessary)

        try {
            String row;
            while ((row = bufferedReader.readLine()) != null) {
                String[] data = row.split(";");

                if (drawings != null) {
                    boolean flag = false;
                    for (String drawing : drawings) {
                        if (drawing.equals(data[3])) {
                            flag = true;
                        }
                    }

                    if (!flag) {
                        continue;
                    }
                }

                // Checking if String from "RepID" already exists for both nodes
                existsAndPutPipe(graph, map, data, 0, 1, 3);
                existsAndPut(graph, map, data, 4, 5, 7, 3, 7);

                // Checking the type of connection: B -> A, A -> B or A <-> B, respectively
                if (data[6].equals("From") || data[6].equals("In")) {
                    graph.addEdge(map.get(data[5]), map.get(data[1])); // Add Edge from B -> A
                } else if (data[6].equals("To") || data[6].equals("Out") || data[6].equals("OPC")) {
                    graph.addEdge(map.get(data[1]), map.get(data[5])); // Add Edge from A -> B
                } else {
                    graph.addEdge(map.get(data[5]), map.get(data[1])); // Add Edge from B -> A
                    graph.addEdge(map.get(data[1]), map.get(data[5])); // Add Edge from A -> B
                }
            }
        } catch (IOException e) {
            log.warning("An exception has occurred while trying to read the file");

            return false;
        }

        log.info("Done building the graph with " + graph.vertexSet().size() + " vertexes and " + graph.edgeSet().size()
                + " edges!");
        return true;
    }

    /*
     * The main algorithm
     * 
     * @param source The node that will be explored.
     * 
     * @param graph The graph that is being searched.
     * 
     * @param subgraph The subgraph that is being created from the source graph.
     * 
     * @param memory All edges that have been visited.
     * 
     * @return Returns true whether there is a path between source and target or if
     * a node is connected to a path node and returns false if it does not.
     */
    public Boolean DFS(NavalNode source, Graph graph, Graph subgraph, Set<Integer> memory) {

        Set<NavalNode> vertexSet = subgraph.vertexSet();
        if (vertexSet.contains(source)) {
            return true;
        }

        Boolean isPath = false;
        for (NavalEdge edge : graph.outgoingEdgesOf(source)) {
            if (!memory.contains(edge.hashCode())) {
                memory.add(edge.hashCode());
                NavalNode target = graph.getEdgeTarget(edge);
                if (DFS(target, graph, subgraph, memory)) {
                    subgraph.addVertex(source);
                    if (!subgraph.containsEdge(source, target)) {
                        subgraph.addEdge(source, target);
                    }
                    isPath = true;
                }
            }
        }

        return isPath;
    }
}
