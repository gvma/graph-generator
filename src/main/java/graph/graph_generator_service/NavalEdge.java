package graph.graph_generator_service;

import org.jgrapht.graph.DefaultEdge;

// // Represents the edge to be used in the graph
public class NavalEdge extends DefaultEdge {

    private static final long serialVersionUID = 1L;
    private String label;
    private int hash = 0;

    @Override
    // Standard toString method
    public String toString() {
        if (label == null) {
            NavalNode source = (NavalNode) getSource();
            NavalNode target = (NavalNode) getTarget();
            label = source.getVertexID().toString() + ":" + target.getVertexID().toString();
        }
        return label;
    }

    // Standard hashCode method
    public int hashCode() {
        if (hash == 0) {
            hash = toString().hashCode();
        }
        return hash;
    }

}