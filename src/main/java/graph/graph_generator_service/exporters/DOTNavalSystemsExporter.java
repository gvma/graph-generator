package graph.graph_generator_service.exporters;

import org.jgrapht.nio.dot.DOTExporter;

import graph.graph_generator_service.Graph;
import graph.graph_generator_service.NavalEdge;
import graph.graph_generator_service.NavalNode;
import lombok.extern.java.Log;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import org.jgrapht.nio.Attribute;
import org.jgrapht.nio.DefaultAttribute;

@Log
// A class that exports the graph to a .dot file
public class DOTNavalSystemsExporter implements NavalSystemsExporter {

    // A function to create an attribute
    public void setAttr(Map<String, Attribute> map, String key, String value) {
        map.put(key, DefaultAttribute.createAttribute(value));
    }

    /*
     * Helper that exports DOT file.
     * @param subgraph The graph to be exported to .dot file;
     * @param path The path that the .dot file will be saved.
     * @return Nothing.
     */
    public String export(Graph subgraph) {
        DOTExporter<NavalNode, NavalEdge> dotExporter = new DOTExporter<>();
        dotExporter.setVertexAttributeProvider((NavalNode v) -> {
            Map<String, Attribute> map = new HashMap<String, Attribute>();

            String tagPipeLabel = v.getTagPipe(); // + " [" + v.getVertexID() + "]";
            setAttr(map, "label", tagPipeLabel);

            if (!v.isPipe()) {
                setAttr(map, "shape", "box");
            }

            if (v.getPipeType().equals("OPC")) {
                setAttr(map, "shape", "diamond");
                setAttr(map, "color", "grey");
                setAttr(map, "style", "filled");
            }

            if (subgraph.getInputs().contains(v.getKey())) {
                setAttr(map, "style", "filled");
                setAttr(map, "fillcolor", "green");
            }

            if (subgraph.getOutputs().contains(v.getKey())) {
                setAttr(map, "style", "filled");
                setAttr(map, "fillcolor", "#00d4ff");
            }

            return map;
        });
        log.info("Writing DOT file!");
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        dotExporter.exportGraph(subgraph, byteArrayOutputStream);
        return byteArrayOutputStream.toString();
    }
}