package graph.graph_generator_service.exporters;

import graph.graph_generator_service.Graph;

// A helper interface to create graph exporters
public interface NavalSystemsExporter {

    // Helper that exports to a String.
    String export(Graph subgraph);

}