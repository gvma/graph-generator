package graph.graph_generator_service.exporters;

import java.util.*;

import graph.graph_generator_service.EquipmentKey;
import graph.graph_generator_service.NavalNode;
import graph.graph_generator_service.Graph;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;

@Data
@Log
@Component
@RequiredArgsConstructor
public class PipenetExporter implements NavalSystemsExporter {

    private JSONNavalSystemsExporter jsonExporter = new JSONNavalSystemsExporter();
    private JSONObject jsonObject;
    private JSONObject data;
    private String analysisTitle = "";
    private JSONArray inputs, outputs;

    public PipenetExporter(String analysisTitle) {
        this.analysisTitle = analysisTitle;
    }

    public void readJSON(String jsonStr) {
        try {
            this.data = new JSONObject(jsonStr);
            this.inputs = this.data.getJSONArray("inputs");
            this.outputs = this.data.getJSONArray("outputs");
        } catch (JSONException exception) {
            log.warning(exception.getMessage());
        }
    }

    public void changeVertexID(Graph subgraph) {
        long id = 1;
        for (NavalNode node : subgraph.vertexSet()) {
            node.setVertexID(id++);
        }
    }

    public void countFittings(Map<String, Integer> map, Set<NavalNode> fittings) {
        for (NavalNode fitting : fittings) {
            String fixedTagPipe = fitting.getTagPipe().replaceAll("[0-9]", "");
            fixedTagPipe = fixedTagPipe.replace("-", "");
            if (map.get(fixedTagPipe) == null) {
                map.put(fixedTagPipe, 1);
            } else {
                Integer count = map.get(fixedTagPipe);
                map.put(fixedTagPipe, count + 1);
            }
        }
    }

    public void fixFittingsNames(StringBuilder fittingsToBuild, NavalNode source, NavalNode target) {
        Map<String, Integer> fittingsCount = new HashMap<>();
        countFittings(fittingsCount, source.getFittings());
        countFittings(fittingsCount, target.getFittings());

        Set<String> fittings = new HashSet<>();

        setFittings(fittingsToBuild, source, fittingsCount, fittings);

        setFittings(fittingsToBuild, target, fittingsCount, fittings);
    }

    public void setFittings(StringBuilder fittingsToBuild, NavalNode source, Map<String, Integer> fittingsCount, Set<String> fittings) {
        for (NavalNode fitting : source.getFittings()) {
            String fixedTagPipe = fitting.getTagPipe().replaceAll("[0-9]", "");
            fixedTagPipe = fixedTagPipe.replace("-", "");
            if (!fittings.contains(fixedTagPipe)) {
                fittingsToBuild.append(String.format("                    <Fitting count=\"%d\" type=\"%s\"/>\n", fittingsCount.get(fixedTagPipe), fixedTagPipe));
                fittings.add(fixedTagPipe);
            }
        }
    }

    private String getNodeIo( JSONObject node ) throws JSONException {
        String tagPipe = node.getString("tagPipe");
        // Checking inputs
        for(int i = 0; i < inputs.length(); ++i ) {
            if ( inputs.getJSONObject(i).getString("tagPipe").equals( tagPipe ) ) {
                return "Input";
            }
        }
        // Checking outputs
        for(int i = 0; i < outputs.length(); ++i ) {
            if ( outputs.getJSONObject(i).getString("tagPipe").equals( tagPipe ) ) {
                return "Output";
            }
        }
        return "No";
    }

    public String export(Graph subgraph) {
        changeVertexID(subgraph);
        readJSON( jsonExporter.export(subgraph) );
        try {
            JSONArray nodes = this.data.getJSONArray("nodes");
            JSONArray edges = this.data.getJSONArray("edges");

            StringBuilder nodesOutput = new StringBuilder();
            nodesOutput.append("\n");
            for (int i = 0; i < nodes.length(); ++i) {
                String vertexID = nodes.getJSONObject(i).getString("vertexID");
                String ionode = getNodeIo(nodes.getJSONObject(i));
                nodesOutput.append(String.format("            <Node label=\"%s\" io-node=\"%s\"></Node>\n", vertexID, ionode));
            }

            StringBuilder edgesOutput = new StringBuilder();
            edgesOutput.append("\n");
            for (int i = 0; i < edges.length(); ++i) {
                String sourceID = edges.getJSONObject(i).getString("source");
                String targetID = edges.getJSONObject(i).getString("target");

                String tagPipeSource = "", tagPipeTarget = "";
                for (int j = 0; j < nodes.length(); ++j) {
                    if (nodes.getJSONObject(j).getString("vertexID").equals(sourceID)) {
                        tagPipeSource = nodes.getJSONObject(j).getString("tagPipe");
                    }
                    if (nodes.getJSONObject(j).getString("vertexID").equals(targetID)) {
                        tagPipeTarget = nodes.getJSONObject(j).getString("tagPipe");
                    }
                }

                NavalNode sourceNode = subgraph.findNode(new EquipmentKey(tagPipeSource));
                NavalNode targetNode = subgraph.findNode(new EquipmentKey(tagPipeTarget));

                StringBuilder fittingsToBuild = new StringBuilder();
                StringBuilder allFittings = new StringBuilder();

                fixFittingsNames(fittingsToBuild, sourceNode, targetNode);

                if (!fittingsToBuild.toString().equals("")) {
                    allFittings.append(String.format("                <Fittings>\n%s                </Fittings>\n", fittingsToBuild.toString()));
                }

                edgesOutput.append(String.format("            <Pipe input=\"%s\" inputTag=\"%s\" output=\"%s\" outputTag=\"%s\">\n%s            </Pipe>\n",
                    sourceID, tagPipeSource.replace("\"", ""), targetID, tagPipeTarget.replace("\"", ""), allFittings.toString()));
            }

            StringBuilder output = new StringBuilder();
            output.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            output.append("<!DOCTYPE Project SYSTEM \"standard.dtd\">\n");
            output.append("<Project version=\"1.9  (0)\">\n");
            output.append("    <Network-standard>\n");
            output.append("        <Libraries>\n");
            output.append("            <System-lib file=\"condição 1 - normal - proj hidráulico.slf\"/>\n");
            output.append("        </Libraries>\n");
            output.append("        <Attributes>\n");
            output.append("            <Units flowtype=\"volumetric\" type=\"user-defined\">\n");
            output.append("                <Length-unit display=\"1\" precision=\"1\" unit=\"meters\"/>\n");
            output.append("                <Diameter-unit display=\"1\" precision=\"3\" unit=\"inches\"/>\n");
            output.append("                <Pressure-unit display=\"1\" precision=\"1\" unit=\"mm-of-water\"/>\n");
            output.append("                <Temperature-unit display=\"1\" precision=\"1\" unit=\"deg-c\"/>\n");
            output.append("                <Velocity-unit display=\"1\" precision=\"1\" unit=\"m-s\"/>\n");
            output.append("                <Density-unit display=\"1\" precision=\"1\" unit=\"kg-m3\"/>\n");
            output.append("                <Viscosity-unit display=\"1\" precision=\"1\" unit=\"cp\"/>\n");
            output.append("                <Volumetric-flow-unit display=\"1\" precision=\"1\" unit=\"m3-hour\"/>\n");
            output.append("                <Mass-flow-unit display=\"1\" precision=\"1\" unit=\"kg-s\"/>\n");
            output.append("                <Heat-capacity-unit display=\"1\" precision=\"1\" unit=\"joule-kgk\"/>\n");
            output.append("                <Power-unit display=\"0\" precision=\"0\" unit=\"kW\"/>\n");
            output.append("            </Units>\n");
            output.append("        </Attributes>\n");
            output.append(String.format("        <Title>%s</Title>\n", analysisTitle));
            output.append(String.format("        <Network-description>%s</Network-description>\n", "description"));
            output.append(String.format("        <Nodes>%s        </Nodes>\n", nodesOutput.toString()));
            output.append(String.format("        <Links>%s        </Links>\n", edgesOutput.toString()));
            output.append("    </Network-standard>\n");
            output.append("</Project>");
            
            return output.toString();
        } catch (JSONException exception) {
            log.warning(exception.getMessage());
            return null;
        }
    }
}