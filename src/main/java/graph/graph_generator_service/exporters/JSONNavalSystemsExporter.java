package graph.graph_generator_service.exporters;

import com.fasterxml.jackson.databind.ObjectMapper;

import graph.graph_generator_service.EquipmentKey;
import graph.graph_generator_service.Graph;
import graph.graph_generator_service.NavalNode;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.Data;
import lombok.Getter;
import lombok.extern.java.Log;

import java.util.HashMap;
import java.util.LinkedList;

@Log
// A class that exports the graph to a String
public class JSONNavalSystemsExporter implements NavalSystemsExporter {

    private final ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);

    // Default Constructor.
    public JSONNavalSystemsExporter() {}

    // Standard get method.
    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    /*
     * Helper that exports a JSON file.
     * @param subgraph The graph to be exported to .dot file;
     * @param path The path that the .dot file will be saved.
     * @return Nothing.
     */
    public String export(Graph subgraph) {
        log.info("Writing JSON file!");
        try {
            JSONNavalSystemsExporter jsonOutput = new JSONNavalSystemsExporter();

            LinkedList<EquipmentKey> inputs = new LinkedList<>();
            LinkedList<EquipmentKey> outputs = new LinkedList<>();
            inputs.addAll(subgraph.getInputs());
            outputs.addAll(subgraph.getOutputs());

            LinkedList<JSONNavalNodeOutput> nodeList = new LinkedList<>();
            for (NavalNode navalNode : subgraph.vertexSet()) {
                nodeList.add(new JSONNavalNodeOutput(navalNode));
            }

            LinkedList<JSONEdgeOutput> edgeOutputs = new LinkedList<>();
            for (NavalNode u : subgraph.vertexSet()) {
                for (NavalNode v : subgraph.getTargetList(u)) {
                    if (subgraph.containsEdge(u, v)) {
                        edgeOutputs.add(new JSONEdgeOutput(u.getPipeType(), u.getVertexID()
                                .intValue(), "Directed", v.getVertexID().intValue(), "Directed"));
                    }
                }
            }

            ObjectMapper objectMapper = jsonOutput.getObjectMapper();
            HashMap<String, Object> map = new HashMap<>();

            map.put("inputs", inputs);
            map.put("outputs", outputs);
            map.put("nodes", nodeList);
            map.put("edges", edgeOutputs);
            return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(map);
        } catch (Exception e) {
            log.info("An error has occurred while trying to process the JSON file!");
            return null;
        }
    }

    @Data
    // Helper class to create the .json file in nodes attribute.
    private class JSONNavalNodeOutput {
        private String repID;
        private String pipeType;
        private String type;
        private Long vertexID;
        private String tagPipe;
        private String drawing;
        private Boolean isPipe;

        // Sole constructor. Used to create the correct .json nodes format attribute.
        public JSONNavalNodeOutput(NavalNode navalNode) {
            this.repID = navalNode.getRepID();
            this.pipeType = navalNode.getPipeType();
            this.type = navalNode.getType();
            this.vertexID = navalNode.getVertexID();
            this.tagPipe = navalNode.getTagPipe();
            this.drawing = navalNode.getDrawing();
            this.isPipe = navalNode.isPipe();
        }
    }

    @Getter
    // Helper class to create the .json file in edges attribute.
    private class JSONEdgeOutput {

        public String is_pipe;
        public int source;
        public String type;
        public int target;
        public String other_type;

        // Sole constructor. Used to create the correct .json edges format attribute.
        public JSONEdgeOutput(String is_pipe, int source, String type, int target, String other_type) {
            this.is_pipe = is_pipe;
            this.source = source;
            this.type = type;
            this.target = target;
            this.other_type = other_type;
        }

    }

}
