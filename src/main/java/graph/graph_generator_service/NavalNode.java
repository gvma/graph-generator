package graph.graph_generator_service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
// Represents the node to be used in the graph.
public class NavalNode implements Cloneable {

    @EqualsAndHashCode.Include
    private EquipmentKey key;

    private String drawing;

    @EqualsAndHashCode.Include
    private String repID;
    private String pipeType;
    private String type;
    private Long vertexID;
    private Set<NavalNode> fittings = new HashSet<>();
    private static String[] types = { "Conditioned Air Return", "Secondary Piping", "Primary Piping", "OPC",
            "Ventilation", "Capillary", "Hydraulic", "Exhaustion", "Connect to Process", "Conditioned Air Supply",
            "Hose", "Pipe-in-Pipe", "Tubing", "Pneumatic Binary" };

    /*
     * Sole constructor. Used to create NavalNodes.
     * @param tagPipe The node's tag pipe.
     * @param repID The node's rep ID.
     * @param pipeType The node's pipe type.
     * @param drawing The drawing that the node is contained.
     * @param type The node's type.
     * @param vertexID The node's ID in the graph's representation.
     */
    public NavalNode(String tagPipe, String repID, String pipeType, String drawing, String type, Long vertexID) {
        this.key = new EquipmentKey(tagPipe);
        this.drawing = drawing;
        this.repID = repID;
        this.pipeType = pipeType;
        this.type = type;
        this.vertexID = vertexID;
    }

    public String getTagPipe() {
        return key.getTagPipe();
    }

    /*
     * A method created to check if a node is a pipe or not, using the attribute types
     * @return Returns true whether the node is contained in the 'types' array and false if it is not.
     */
    public boolean isPipe() {
        for (String type : types) {
            if (type.equals(this.pipeType)) {
                return true;
            }
        }
        return false;
    }

    public boolean isConnector(Graph graph) {
        return  !graph.outputs.contains(this.key) && 
                !graph.inputs.contains(this.key) &&
                this.getPipeType().equals("Connector");
    }

    public boolean isComponent(Graph graph) {
        return  !graph.outputs.contains(this.key) && 
                !graph.inputs.contains(this.key) &&
                this.getType().equals("Inline");
    }

    // Standard clone method.
    public NavalNode clone() {
        return new NavalNode(this.key, this.drawing, this.repID, this.pipeType, this.type, this.vertexID, this.fittings);
    }

    @Override
    // Standard toString method.
    public String toString() {
        return String.format("[id: %d, tag: %s, drawing: %s, repID: %s, pipeType: %s]", vertexID, getTagPipe(),
                getDrawing(), getRepID(), getPipeType());
    }

    /*
     * @param otherKey A key to be compared with this object's key
     * @return Returns true whether the key is equal to the other key, false if it is not.
     */
    public boolean checkKey(EquipmentKey otherKey) {
        return this.key.equals(otherKey);
    }

}